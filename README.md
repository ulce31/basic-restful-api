# Basic RESTful API
## Summary

- Objective: Extend [your Node.js server](https://gitlab.com/ulce31/basic-nodejs-server) to create a RESTful API with CRUD operations (Create, Read, Update, Delete) for a specific resource.

## Requirements
- Having NodeJS (13.2.0 or a newer version - "type": "module" in package.json)
- Having [yarn](https://yarnpkg.com/getting-started/install)
- Having a requesting tool ([curL](https://curl.se/), [Postman](https://www.postman.com/)...)
## Make it work 👨‍🔬
- [From GitLab] Clone the project
- [In your shell, in the project folder] ⬇️
```sh 
  yarn install
  node server.js
```
